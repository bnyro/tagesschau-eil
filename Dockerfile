FROM golang:alpine AS build
WORKDIR /app

COPY go.mod go.sum ./
RUN go mod download

COPY . ./
RUN CGO_ENABLED=0 GOOS=linux go build -o "/ts-eil"

FROM alpine
COPY --from=build "/ts-eil" "/ts-eil"
COPY --from=build "/app/static" "/static"

EXPOSE 5050
CMD ["/ts-eil"]
